﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqlBackupAndRestore
{
    public partial class FrmBackup : Form
    {
        public string strConnection { get; set; }
        public FrmBackup()
        {
            InitializeComponent();
            //默认数据库名称和备份路径
            txtDBName.Text = "northwind";
            txtBackupPath.Text = @"c:\dbbackup";
            strConnection = ConfigurationManager.ConnectionStrings["sqlserver"].ConnectionString;
            GetBackupFiles(txtBackupPath.Text);
        }

        /// <summary>
        /// 选择数据库备份路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择数据库备份所在文件夹";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (string.IsNullOrEmpty(dialog.SelectedPath))
                {
                    MessageBox.Show(this, "文件夹路径不能为空", "提示");
                    return;
                }
                txtBackupPath.Text = dialog.SelectedPath;
                GetBackupFiles(txtBackupPath.Text);
            }
        }

        /// <summary>
        /// 获取所有数据库备份
        /// </summary>
        /// <param name="path"></param>
        private void GetBackupFiles(string path)
        {
            string[] files = Directory.GetFiles(path, "*.bak");
            lbBackups.Items.Clear();
            lbBackups.Items.AddRange(files);
        }

        /// <summary>
        /// 数据库备份
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBackup_Click(object sender, EventArgs e)
        {
            //数据库名称
            if (string.IsNullOrEmpty(txtDBName.Text))
            {
                MessageBox.Show("请输入需要备份的数据库名称!","提示",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            if (string.IsNullOrEmpty(txtBackupPath.Text))
            {
                MessageBox.Show("请输入存放备份的目录!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (!Directory.Exists(txtBackupPath.Text))
            {
                MessageBox.Show("路径不存在");
                return;
            }
            else
            {
                //yyyyMMddHHmmss为24小时制，yyyyMMddhhmmss为12小时制
                string backFile = txtDBName.Text + DateTime.Now.ToString("yyyyMMddHHmmss") + ".bak";
                DbBackupHelper.BackupDataBase(strConnection, txtDBName.Text, txtBackupPath.Text, backFile);
                GetBackupFiles(txtBackupPath.Text);
                MessageBox.Show("备份成功", "结果", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        /// <summary>
        /// 数据库还原
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDBName.Text))
            {
                MessageBox.Show("请先输入数据库名称", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (lbBackups.SelectedItem == null)
            {
                MessageBox.Show("请先选择需要还原的备份文件!");
            }
            else if (!File.Exists(lbBackups.SelectedItem.ToString()))
            {
                MessageBox.Show("备份文件不存在!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DbBackupHelper.RestoreDataBase(strConnection,txtDBName.Text, lbBackups.SelectedItem.ToString());
                MessageBox.Show("数据库还原成功！", "结果", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
